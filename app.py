import flask
from flask import Flask, render_template
import sklearn
from sklearn.multioutput import MultiOutputRegressor
from sklearn.preprocessing import MinMaxScaler
import pickle
import pandas as pd
import numpy as np

app = flask.Flask(__name__, template_folder = 'templates')

@app.route('/', methods = ['GET', 'POST'])

@app.route('/index', methods = ['GET', 'POST'])
def index():
    if flask.request.method == 'GET':
        return render_template('index.html')

    if flask.request.method == 'POST':
        with open('model.pkl', 'rb') as f:
            model_load = pickle.load(f)

        input_iw = float(flask.request.form['iw'])
        input_if = float(flask.request.form['if'])
        input_vw = float(flask.request.form['vw'])
        input_fp = float(flask.request.form['fp'])

        input_data = [[input_iw, input_if, input_vw, input_fp]]
        scaler = MinMaxScaler()
        input_data_n = scaler.transform(input_data)

        features = pd.DataFrame(input_data_n, index=[0])
        return features

        Y_pred_input = model_test.predict(features)

        context = {
            'iw': input_iw,
            'if': input_if,
            'vw': input_vw,
            'fp': input_fp,
            'depth': round(Y_pred_input[0][0], 2),
            'width': round(Y_pred_input[0][1], 2)
        }

        return render_template('index.html', **context)

    if __name__ == '__index__':
        app.run(debug=True)